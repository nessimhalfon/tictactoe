//
//  Extension.swift
//  TicTacToe
//
//  Created by Nessim Halfon on 12/11/2019.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

//MARK:-
extension TimeInterval {
    func timeString() -> String {
        let minutes = Int(self) / 60 % 60
        let seconds = Int(self) % 60
        
        return String(format:"%02i:%02i", minutes, seconds)
    }
}

//MARK:-
extension ViewController {
    
    func displayAlert(tilte: String?, message: String?, completion: (()-> Void)? = nil, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: tilte, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: handler)
        alert.addAction(action)
        present(alert, animated: true, completion: completion)
    }
    
}
