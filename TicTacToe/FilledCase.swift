//
//  FilledCase.swift
//  TicTacToe
//
//  Created by Nessim Halfon on 12/11/2019.
//  Copyright © 2019 Nessim Halfon. All rights reserved.
//

import Foundation

enum FilledCase: Int {
    case empty = 0
    case playerOne = 1
    case playerTwo = 2
}
