//
//  ViewController.swift
//  TicTacToe
//
//  Created by Nessim Halfon on 12/11/2019.
//  Copyright © 2019 Nessim Halfon. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var scorePlayerOneLabel: UILabel!
    @IBOutlet weak var scorePlayerTwoLabel: UILabel!
    @IBOutlet weak var isPlayerOneImageView: UIImageView!
    @IBOutlet weak var isPlayerTwoImageView: UIImageView!
    @IBOutlet weak var winnerLabel: UILabel!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var boardView: UIView!
    @IBOutlet weak var startButton: UIButton!
    
    //MARK:- Properties
    var playerOne = true
    var playerTwo = false
    
    var scorePlayerOne = 0
    var scorePlayerTwo = 0
    
    // Array pour savoir si une case est rempli par le joueur 1 ou 2, 0 pour une case vide.
    var filledArray: [Int] = {
        var array = [Int]()
        for _ in 0...8 {
            array.append(FilledCase.empty.rawValue)
        }
        return array
    }()
    
    var countMoves = 0 // If == 9 -> Draw
    
    private var startTime = 180.0
    private var startTimer = Timer()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
       setupPreviewGame()
    }
    
    //MARK:- Func View
    
    private func setupPreviewGame() {
        playerView.isHidden = true
        boardView.isHidden = true
        winnerLabel.text = nil
        timerLabel.text = nil
        scorePlayerOne = 0
        scorePlayerTwo = 0
        setupStartButton()
    }
    
    private func setupStartButton() {
        startButton.layer.borderWidth = 2
        startButton.layer.borderColor = UIColor.black.cgColor
        startButton.isHidden = false
    }
    
    private func setupGame() {
        playerView.isHidden = false
        boardView.isHidden = false
        isPlayerOneImageView.isHidden = false
        isPlayerTwoImageView.isHidden = true
        scorePlayerOneLabel.text = "\(scorePlayerOne)"
        scorePlayerTwoLabel.text = "\(scorePlayerTwo)"
        winnerLabel.text = nil
        timerLabel.text = "Time : 3:00."
        startButton.isHidden = true
    }
    
    private func isPlayerOne() {
        isPlayerOneImageView.isHidden = true
        isPlayerTwoImageView.isHidden = false
    }
      
    private func isPlayerTwo() {
        isPlayerTwoImageView.isHidden = true
        isPlayerOneImageView.isHidden = false
    }
    
    //MARK:- Func Reset Game
    
    /// Permet de reset le jeu.
    private func resetGame() {
        resetBoard()
        playerOne = true
        playerTwo = false
        isPlayerOneImageView.isHidden = false
        isPlayerTwoImageView.isHidden = true
        countMoves = 0
    }
    
    /// Permet de remettre a 0 les cases du plateau.
    private func resetBoard() {
        for i in 1...9 {
            filledArray[i-1] = FilledCase.empty.rawValue
            let button = view.viewWithTag(i) as? UIButton
            button?.setBackgroundImage(nil, for: .normal)
        }
    }
    
    /// Si on a un gagnant le jeu se remet a 0.
    private func resetIfWinner() {
        if let winner = checkForWinner() {
            winnerLabel.text = "The last Winner is : Player \(winner)"
            addScore(for: winner)
            resetGame()
        }
    }
     
     /// Si pas de gagnant le jeu se remet a 0.
     private func resetIfDraw() {
         checkForDraw()
     }
    
    // MARK:- Func Check
    
    /// Permet de vérifié si on a un gagnant.
    /// Retourne le gagnant
    private func checkForWinner() -> Int? {
        var winner:Int? = nil
    
        // Test Ligne
        if filledArray[0] != 0 && filledArray[0] == filledArray[1] && filledArray[1] == filledArray[2] {
            winner = filledArray[0]
        }
        if filledArray[3] != 0 && filledArray[3] == filledArray[4] && filledArray[4] == filledArray[5] {
            winner = filledArray[3]
        }
        if filledArray[6] != 0 && filledArray[6] == filledArray[7] && filledArray[7] == filledArray[8] {
            winner = filledArray[6]
        }
        
        // Test Colonne
        if filledArray[0] != 0 && filledArray[0] == filledArray[3] && filledArray[3] == filledArray[6] {
            winner = filledArray[0]
        }
        if filledArray[1] != 0 && filledArray[1] == filledArray[4] && filledArray[4] == filledArray[7] {
            winner = filledArray[1]
        }
        if filledArray[2] != 0 && filledArray[2] == filledArray[5] && filledArray[5] == filledArray[8] {
            winner = filledArray[2]
        }
        
        // Test Diagonal
        if filledArray[0] != 0 && filledArray[0] == filledArray[4] && filledArray[4] == filledArray[8] {
            winner = filledArray[0]
        }
        if filledArray[2] != 0 && filledArray[2] == filledArray[4] && filledArray[4] == filledArray[6] {
            winner = filledArray[2]
        }
    
        return winner
    }

    private func checkForDraw() {
        if countMoves == 9 {
            winnerLabel.text = "There are no winners"
            resetGame()
        }
    }
    
    private func theWinnerIs() -> String {
        if scorePlayerOne > scorePlayerTwo {
            return "Player 1"
        } else if scorePlayerOne < scorePlayerTwo {
            return "Player 2"
        } else {
            return "Égalité"
        }
    }
    
    //MARK:- Func Moves
    
    /// Permet de remplir une case par le joueur avec un cercle ou une croix.
    private func setMove(forPlayer player: Int, button: UIButton) {
        if player == FilledCase.playerOne.rawValue {
            button.setBackgroundImage(UIImage(named:"blueCircle"), for: .normal)
        } else if player == FilledCase.playerTwo.rawValue {
            button.setBackgroundImage(UIImage(named:"redCross"), for: .normal)
        }
    }
    
    private func switchPlayer() {
        playerOne = !playerOne
        playerTwo = !playerTwo
    }
    
    //MARK:- Timer Func
    private func runStartTimer() {
        startTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateStartTimer), userInfo: nil, repeats: true)
        setupGame()
    }
    
    @objc private func updateStartTimer() {
        DispatchQueue.main.async {
            self.timerLabel.text = "Time : \(self.startTime.timeString())."
        }
        startTime -= 1.0
        
        if startTime == 0 {
            let winner = "The winner is " + theWinnerIs()
            let scoreFinal = "\n\nScores : \n Player1 : \(scorePlayerOne) \n Player2 : \(scorePlayerTwo)"
            let message = winner + scoreFinal
            displayAlert(tilte: "Game Over", message: message)
            resetTimer()
            setupPreviewGame()
            resetGame()
        }
    }
    
    private func resetTimer() {
        startTime = 180.0
        startTimer.invalidate()
    }
    
    //MARK:- Func Game
    
    private func addScore(for winner: Int) {
        if winner == FilledCase.playerOne.rawValue {
            scorePlayerOne += 1
            scorePlayerOneLabel.text = "\(scorePlayerOne)"
        } else if winner == FilledCase.playerTwo.rawValue {
            scorePlayerTwo += 1
            scorePlayerTwoLabel.text = "\(scorePlayerTwo)"
       }
    }
    
    private func gameInProgress(button: UIButton) {
        if filledArray[button.tag-1] == FilledCase.empty.rawValue {
            if playerOne == true && playerTwo == false {
                isPlayerOne()
                setMove(forPlayer: FilledCase.playerOne.rawValue, button: button)
                filledArray[button.tag-1] = FilledCase.playerOne.rawValue
                countMoves += 1
            } else if playerTwo == true && playerOne == false {
                isPlayerTwo()
                setMove(forPlayer: FilledCase.playerTwo.rawValue, button: button)
                filledArray[button.tag-1] = FilledCase.playerTwo.rawValue
                countMoves += 1
            }
            
            switchPlayer()
            
            resetIfDraw()
            resetIfWinner()
            
        } else {
            print("Button already used.")
        }
    }
    
    //MARK:- IBAction
    
    @IBAction func startGame() {
        runStartTimer()
    }
    
    
    @IBAction func buttonDidTap(_ sender: UIButton) {
        gameInProgress(button: sender)
    }
}
