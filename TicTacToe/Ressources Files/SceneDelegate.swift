//
//  SceneDelegate.swift
//  TicTacToe
//
//  Created by Nessim Halfon on 12/11/2019.
//  Copyright © 2019 Nessim Halfon. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
    }
}

